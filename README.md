# KEDA-Demo

## Helm installation

1. Add helm repository

       helm add repo kedacore https://kedacore.github.io/charts
       helm repo update

2. To install helm chart
        
       helm install keda kedacore/keda --namespace keda --create-namespace
    
## Auto-scaleing Guide

To scale a Kubernetes `Deployment` or `Statefulset` or `Pod` you need to first create a ScaledObject that can be applied to Kubernetes.

```yaml
apiVersion: keda.sh/v1alpha1
kind: ScaledObject
metadata:
  name: {scaled-object-name}
  namespace: {namespace-of-resource}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: {Deployment|Statefulet}  # Kind of resource
    name: {resource-name}
  pollingInterval: 30              # Trigger polling interval
  cooldownPeriod:  300             # Cooldown period before scaling down
  minReplicaCount: 0
  maxReplicaCount: 5
  triggers:
  # {list of triggers to activate the deployment}
```

### RabbitMQ Trigger

```yaml
triggers:
- type: rabbitmq
  metadata:
    host: amqp://<username>:<password>@rabbitmq-headless.default.svc.cluster.local:5672/
    protocol: amqp
    queueName: {queue-name}
    queueLength: "20"              # The Queue length must be wrapped in " " to define the value as String
```

Replace the `<username>`, `<password>`, and `<queue_name>` with your required values.

>Note: Beaware when adding paswords with special characters to passwords. They may interfere with URL